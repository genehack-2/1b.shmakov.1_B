#!/bin/perl -w

use strict;
use Bio::SeqIO;
use Bio::Tools::Run::StandAloneBlast;

print "FASTA File and Path to database\n";
my $bdb = $ARGV[1];
my @params = (
	-'database' => $bdb,
	-'program' => 'blastx',
	-'e' => '1e-8'
#	-'a' => 2
);

# ABYSS k-mere length
my $kmer = 15;

my $file = $ARGV[0];
my $seq_io = Bio::SeqIO -> new(-file => $file);

my $blast_search = Bio::Tools::Run::StandAloneBlast -> new(@params);

print "QueryID\tEvalue\tHitLength\tHitID\n";
open OUT, ">", 'Output_FoundReads.fa';
my %data = ();
my $count = 0;
while (my $seq_object = $seq_io -> next_seq()) {
	my $query_id = $seq_object -> id();
#	print $query_id, "\n";
	$count = $count + 1;
	print STDERR "  Processed ", $count, "\n" if ($count % 1000 == 0);
	my $seq = $seq_object;
	my $search = $blast_search -> blastall($seq);
	while (my $result = $search -> next_result()) {
		my $printed = 0;
		while(my $hit = $result -> next_hit()) {
			if ($printed == 0) {
				$data{$query_id} = {
					query => $seq -> id(),
					sig => $hit -> significance(),
					qr_len => $seq_object -> length(),
					hit_len => $hit -> length(),
					hit_id => $hit -> name()
				};
				print $data{$query_id}{'query'}, "\t";
				print $data{$query_id}{'sig'}, "\t";
				print $data{$query_id}{'hit_len'}, "\t";
				print $data{$query_id}{'hit_id'}, "\n";
				print OUT ">", $data{$query_id}{'query'}, "\n", $seq -> seq(), "\n";
			}
#			print " ", $hit -> name(), "\n";
			$printed = 1;
		}
	}
}
close OUT;

system('ABYSS -k '.$kmer.' Output_FoundReads.fa -o Output_AssembledGene.fa');
