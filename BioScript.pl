#!/bin/perl -w
use strict;
use Bio::SeqIO;
use Bio::Tools::Run::StandAloneBlast;
print "FASTA File and Path to database\n";

my $minimal_length = 400;
my $minimal_homology = 40;
my $maximal_homology = 99;
my $bdb = $ARGV[1];
my @params = (
	-'database' => $bdb,
	-'e' => '1e-20'
#	-'a' => 2
);
my $file = $ARGV[0];
my $seq_io = Bio::SeqIO -> new(-file => $file);
my $blast_search = Bio::Tools::Run::StandAloneBlast -> new(@params);

print "QueryID\tEvalue\tQueryLength\tHitLength\thomology\tHitID\n";
open OUT, ">", 'Output_FoundProteins.fa';
my %data = ();
my $count = 0;
while (my $seq_object = $seq_io -> next_seq()) {
	my $query_id = $seq_object -> id();
#	print $query_id, "\n";
	if ($seq_object -> length() >= $minimal_length) {
		my $seq = $seq_object;
		$count = $count + 1;
		print STDERR "  Processed ", $count, "\n" if ($count % 1000 == 0);
		my $search = $blast_search -> blastpgp($seq);
		while (my $result = $search -> next_result()) {
			my $printed = 0;
			while(my $hit = $result -> next_hit()) {
				if ($printed == 0) {
					my $homology = scalar ($hit -> seq_inds('query', 'conserved')) / $seq_object -> length() * 100;
					$homology = sprintf('%.1f', $homology);
					if ($homology >= $minimal_homology and $homology <= $maximal_homology) {
						$data{$query_id} = {
							query => $seq -> id(),
							sig => $hit -> significance(),
							hom => $homology,
							qr_len => $seq_object -> length(),
							hit_len => $hit -> length(),
							hit_id => $hit -> name()
						};
						print $data{$query_id}{'query'}, "\t";
						print $data{$query_id}{'sig'}, "\t";
						print $data{$query_id}{'qr_len'}, "\t";
						print $data{$query_id}{'hit_len'}, "\t";
						print $data{$query_id}{'hom'}, "\t";
						print $data{$query_id}{'hit_id'}, "\n";
						print OUT ">", $data{$query_id}{'query'}, "\n", $seq -> seq(), "\n";
#						print ">", $data{$query_id}{'query'}, "\n", $seq -> seq(), "\n";

					}
				}
#				print " ", $hit -> name(), "\n";
				$printed = 1;
			}
		}
	}
}
close OUT;